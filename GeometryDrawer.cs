﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Csharp3d {
enum ProjectionType {
    Isometric,
    Perspective
}

// draw all 3d objects on canvas
class GeometryDrawer {
    private MainWindow _mainWindow;

    private Pen _penAxis = new Pen(Color.Black);
    private Pen _penLine = new Pen(Color.Green, 3);
    private Pen _penFace = new Pen(Color.Red);
    private Pen _penPoint = new Pen(Color.Blue);

    private CanvasDrawer _canvasDrawer;

    // [x,4]
    private double[,] _loadedVertices = Objects.CubeVertices;

    // [y,3]
    private int[,] _loadedFaces = Objects.CubeFaces;

    public GeometryDrawer(MainWindow mainWindow) {
        _mainWindow = mainWindow;
        _canvasDrawer = new CanvasDrawer(_mainWindow.MyPictureBox);
    }


    public ProjectionType ProjectionType { get; set; } = ProjectionType.Perspective;

    private void AddProjection(TransMatr transMatr) {
        if (ProjectionType == ProjectionType.Isometric) {
            transMatr.AddIsometricProjection();
        } else if (ProjectionType == ProjectionType.Perspective) {
            transMatr.AddPerspectiveProjection();
        }
    }

    private const double AxesLength = 200;

    // [6 x 4]
    private readonly double[,] _axesPoints = {
        {0, 0, 0, 1},
        {AxesLength, 0, 0, 1},
        {0, 0, 0, 1},
        {0, AxesLength, 0, 1},
        {0, 0, 0, 1},
        {0, 0, AxesLength, 1}
    };

    // main transformation matrix
    private TransMatr _transMatr = new TransMatr();

    // cache for axes points [6 x 4]
    double[,] mappedAxes = new double[6, 4];

    public int CenterX => _canvasDrawer.CenterX;
    public int CenterY => _canvasDrawer.CenterY;

    void DrawAxes() {
        _canvasDrawer.DrawCircle(_penPoint, CenterX, CenterY, 5);

        _transMatr.ResetMatrix();
        AddProjection(_transMatr);
        _canvasDrawer.MoveToCenter(_transMatr);
        _transMatr.MultLeftAndNormalize(_axesPoints, mappedAxes);

        _canvasDrawer.DrawLine(mappedAxes, 0, 1, _penAxis);
        _canvasDrawer.DrawLine(mappedAxes, 2, 3, _penAxis);
        _canvasDrawer.DrawLine(mappedAxes, 4, 5, _penAxis);

        _canvasDrawer.DrawText("X", mappedAxes, 1, _penAxis);
        _canvasDrawer.DrawText("Y", mappedAxes, 3, _penAxis);
        _canvasDrawer.DrawText("Z", mappedAxes, 5, _penAxis);
//        Console.WriteLine("axes:");
//        mappedAxes.Print();
    }

    // cache the collection for DrawAll method.
    private HashSet<int> _visiblePointIndices = new HashSet<int>();


    private void SwapFaces(int[,] faces, int i1, int i2) {
        int temp;
        temp = faces[i1, 0];
        faces[i1, 0] = faces[i2, 0];
        faces[i2, 0] = temp;
        
        temp = faces[i1, 1];
        faces[i1, 1] = faces[i2, 1];
        faces[i2, 1] = temp;
        
        temp = faces[i1, 2];
        faces[i1, 2] = faces[i2, 2];
        faces[i2, 2] = temp;
    }

    private double MinZ(int[,] faces, double[,] coords, int i) {
        double z1 = coords[faces[i, 0], 2];
        double z2 = coords[faces[i, 1], 2];
        double z3 = coords[faces[i, 2], 2];
        return Math.Min(Math.Min(z1, z2), z3);
    }

    // qsort
    private void SortFaces(int[,] faces, double[,] coords, int low, int high) {
        int i = low;
        int j = high;
        double pivot = MinZ(faces, coords, (i + j) / 2);

        while (i <= j) {
            while (MinZ(faces, coords, i) < pivot) {
                i++;
            }
            while (MinZ(faces, coords, j) > pivot) {
                j--;
            }
            if (i <= j) {
                SwapFaces(faces, i, j);
                i++;
                j--;
            }
        }
        if (j > low) {
            SortFaces(faces, coords, low, j);
        }
        if (i < high) {
            SortFaces(faces, coords, i, high);
        }
    }

    public void DrawAll() {
        _canvasDrawer.ClearAll();
        DrawAxes();

        _transMatr.ResetMatrix();
        _transMatr.AddScale(_mainWindow.ScaleSlider.Value);

        if (_mainWindow.ChbRotateAfterShift.IsChecked.Equals(true)) {
            _transMatr.AddShift(_mainWindow.MoveXSlider.Value, _mainWindow.MoveYSlider.Value,
                _mainWindow.MoveZSlider.Value);
            _transMatr.AddRotateX(_mainWindow.RotateXSlider.Value);
            _transMatr.AddRotateY(_mainWindow.RotateYSlider.Value);
            _transMatr.AddRotateZ(_mainWindow.RotateZSlider.Value);
        } else {
            _transMatr.AddRotateX(_mainWindow.RotateXSlider.Value);
            _transMatr.AddRotateY(_mainWindow.RotateYSlider.Value);
            _transMatr.AddRotateZ(_mainWindow.RotateZSlider.Value);
            _transMatr.AddShift(_mainWindow.MoveXSlider.Value, _mainWindow.MoveYSlider.Value,
                _mainWindow.MoveZSlider.Value);
        }
        AddProjection(_transMatr);

        double[,] vertProjection = new double[_loadedVertices.L0(), _loadedVertices.L1()];
        _canvasDrawer.MoveToCenter(_transMatr);
        _transMatr.MultLeftAndNormalize(_loadedVertices, vertProjection);


        _visiblePointIndices.Clear();
        // draw faces:

        if (_mainWindow.RbtFaceSorting.IsChecked.Equals(true)) {
            SortFaces(_loadedFaces, vertProjection, 0, _loadedFaces.L0() - 1);
        }
        
        for (int i = 0; i < _loadedFaces.L0(); i++) {
            int i1 = _loadedFaces[i, 0];
            int i2 = _loadedFaces[i, 1];
            int i3 = _loadedFaces[i, 2];

            double angle = FaceAngle(vertProjection, i1, i2, i3);
            bool visible = _mainWindow.ChbRemoveInvisibleFaces.IsChecked.Equals(false) ||
                           angle > 0;
            if (visible) {
                // draw face edges
                if (_mainWindow.ChbFillWithColor.IsChecked.Equals(true)) {
                    _penFace.Color = Color.FromArgb(255, (int) Math.Max(angle * 255, 0), 0, 100);
                    
                    if (_mainWindow.RbtZBuffer.IsChecked.Equals(true)) {
                        _canvasDrawer.DrawTriangleWithZBuffer(vertProjection, i1, i2, i3, _penFace);
                    } else {
                        _canvasDrawer.DrawTriangle(vertProjection, i1, i2, i3, _penFace);
                    }
                } else {
                    _canvasDrawer.DrawLine(vertProjection, i1, i2, _penLine);
                    _canvasDrawer.DrawLine(vertProjection, i2, i3, _penLine);
                    _canvasDrawer.DrawLine(vertProjection, i3, i1, _penLine);
                }
                _visiblePointIndices.Add(i1);
                _visiblePointIndices.Add(i2);
                _visiblePointIndices.Add(i3);
            }
        }
        // draw points
//        foreach (int i in _visiblePointIndices) {
//            _canvasDrawer.DrawPoint(_penPoint, vertProjection, i);
//            _canvasDrawer.DrawText(i.ToString(), vertProjection, i, _penPoint);
//        }
        _canvasDrawer.Submit();
    }

    // returns the face angle. i1 i2 i3 indices of face vertices
    private double FaceAngle(double[,] vertices, int i1, int i2, int i3) {
        double x1 = vertices[i1, 0];
        double y1 = vertices[i1, 1];
        double z1 = vertices[i1, 2];
        double x2 = vertices[i2, 0];
        double y2 = vertices[i2, 1];
        double z2 = vertices[i2, 2];
        double x3 = vertices[i3, 0];
        double y3 = vertices[i3, 1];
        double z3 = vertices[i3, 2];

        // face vectors
        double ax = x2 - x1;
        double ay = y2 - y1;
        double az = z2 - z1;
        double bx = x3 - x1;
        double by = y3 - y1;
        double bz = z3 - z1;

        // normal vector (perpendicular)
        double nX = ay * bz - az * by;
        double nY = az * bx - ax * bz;
        double nZ = ax * by - ay * bx;

        double dotProduct = nX * 0 + nY * 0 + nZ * (-1);
        double angle = dotProduct / Math.Sqrt(nX * nX + nY * nY + nZ * nZ);
//        Console.WriteLine("Visible: " + (dotProduct > 0));
        return angle;
    }

    public void LoadCube() {
        _loadedVertices = Objects.CubeVertices;
        _loadedFaces = Objects.CubeFaces;
    }
public void LoadIntersection() {
        _loadedVertices = Objects.IntersectionVertices;
        _loadedFaces = Objects.IntersectionFaces;
    }

    public void LoadOctahedron() {
        _loadedVertices = Objects.OctahedronVertices;
        _loadedFaces = Objects.OctahedronFaces;
    }

    public void LoadDodecahedron() {
        _loadedVertices = Objects.DodecahedronVertices;
        _loadedFaces = Objects.DodecahedronFaces;
    }

    public void LoadIcosahedron() {
        _loadedVertices = Objects.IcosahedronVertices;
        _loadedFaces = Objects.IcosahedronFaces;
    }

    public void LoadSphere(int radius, int stepsCount) {
        double[,] vertices;
        int[,] faces;
        Objects.GenerateSphere(out vertices, out faces, radius, stepsCount);
        _loadedVertices = vertices;
        _loadedFaces = faces;
    }

    public void LoadGarlic(int radius, int stepsCount) {
        double[,] vertices;
        int[,] faces;
        Objects.GenerateGarlic(out vertices, out faces, radius, stepsCount);
        _loadedVertices = vertices;
        _loadedFaces = faces;
    }

    public void LoadTorus(int radius, int radiusCircle, int stepsCount) {
        double[,] vertices;
        int[,] faces;
        Objects.GenerateTorus(out vertices, out faces, radius, radiusCircle, stepsCount);
        _loadedVertices = vertices;
        _loadedFaces = faces;
    }
    public void LoadSpiral(int radius, int radiusCircle, int stepsCount, int k) {
        double[,] vertices;
        int[,] faces;
        Objects.GenerateSpiral(out vertices, out faces, radius, radiusCircle, k, stepsCount);
        _loadedVertices = vertices;
        _loadedFaces = faces;
    }
}
}