﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Threading;

namespace Csharp3d {
static class Ext {
    public static int L0(this Array matr) => matr.GetLength(0);
    public static int L1(this Array matr) => matr.GetLength(1);

    public static int intOrDefault(this String str, int def) {
        int result;
        if (!int.TryParse(str, out result)) {
            result = def;
        }
        return result;
    }

    public static void Print(this double[,] points) {
        for (int i = 0; i < points.L0(); i++) {
            for (int j = 0; j < points.L1(); j++) {
                Console.Write($"{(int) points[i, j]} ");
            }
            Console.WriteLine();
        }
    }
}


/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow {
    private GeometryDrawer _geometryDrawer;

    public MainWindow() {
        InitializeComponent();
        InitAnimationTimer();

        ScaleSlider.Minimum = 0.5;
        ScaleSlider.Maximum = 2.0;
        ScaleSlider.Value = 1.0;

        // shortcuts
        RoutedCommand resetCommand = new RoutedCommand();
        RoutedCommand animateCommand = new RoutedCommand();
        RoutedCommand isometricCommand = new RoutedCommand();
        RoutedCommand perspectiveCommand = new RoutedCommand();
        RoutedCommand upCommand = new RoutedCommand();
        RoutedCommand leftCommand = new RoutedCommand();
        RoutedCommand rightCommand = new RoutedCommand();
        RoutedCommand downCommand = new RoutedCommand();
        resetCommand.InputGestures.Add(new KeyGesture(Key.R, ModifierKeys.Control));
        animateCommand.InputGestures.Add(new KeyGesture(Key.A, ModifierKeys.Control));
        isometricCommand.InputGestures.Add(new KeyGesture(Key.I, ModifierKeys.Control));
        perspectiveCommand.InputGestures.Add(new KeyGesture(Key.P, ModifierKeys.Control));
        upCommand.InputGestures.Add(new KeyGesture(Key.Up));
        leftCommand.InputGestures.Add(new KeyGesture(Key.Left));
        rightCommand.InputGestures.Add(new KeyGesture(Key.Right));
        downCommand.InputGestures.Add(new KeyGesture(Key.Down));
        MyWindow.CommandBindings.Add(new CommandBinding(resetCommand, BtnResetClick));
        MyWindow.CommandBindings.Add(new CommandBinding(animateCommand, BtnAnimateClick));
        MyWindow.CommandBindings.Add(new CommandBinding(isometricCommand, ClickIsometricProjection));
        MyWindow.CommandBindings.Add(new CommandBinding(perspectiveCommand, ClickPerspectiveProjection));
        MyWindow.CommandBindings.Add(
            new CommandBinding(leftCommand, (sender, args) => UpdateSlider(RotateYSlider, -10)));
        MyWindow.CommandBindings.Add(
            new CommandBinding(rightCommand, (sender, args) => UpdateSlider(RotateYSlider, 10)));
        MyWindow.CommandBindings.Add(new CommandBinding(upCommand, (sender, args) => UpdateSlider(RotateXSlider, 10)));
        MyWindow.CommandBindings.Add(
            new CommandBinding(downCommand, (sender, args) => UpdateSlider(RotateXSlider, -10)));

        _initEnded = true;
        _geometryDrawer = new GeometryDrawer(this);

        _geometryDrawer.DrawAll();
    }

    private void ClickIsometricProjection(object sender, RoutedEventArgs e) {
        RbtIsometric.IsChecked = true;
    }

    private void ClickPerspectiveProjection(object sender, RoutedEventArgs e) {
        RbtPerspective.IsChecked = true;
    }

    // if initialization ended
    private bool _initEnded = false;

    private void BtnResetClick(object sender, RoutedEventArgs e) {
        GeometryDrawer temp = _geometryDrawer;
        _geometryDrawer = null;
        
        ScaleSlider.Value = 1;
        MoveXSlider.Value = 0;
        MoveYSlider.Value = 0;
        MoveZSlider.Value = 0;
        RotateXSlider.Value = 0;
        RotateYSlider.Value = 0;
        RotateZSlider.Value = 0;
        
        _geometryDrawer = temp;
        TbSegments.Text = "10";
        TbRad.Text = "200";
        _geometryDrawer?.DrawAll();
    }


    private void UpdateSlider(Slider slider, double step) {
        if (slider.Value + step > slider.Maximum) {
            slider.Value = slider.Minimum;
        } else if (slider.Value + step < slider.Minimum) {
            slider.Value = slider.Maximum;
        }
        slider.Value += step;
    }

    private readonly DispatcherTimer _animationTimer = new DispatcherTimer();

    private void InitAnimationTimer() {
        _animationTimer.Tick += (o, args) => {
            // remove geometry drawer to perform the changes together.
            GeometryDrawer temp = _geometryDrawer;
            _geometryDrawer = null;
            UpdateSlider(RotateXSlider, 0.3);
            UpdateSlider(RotateYSlider, 1);
            UpdateSlider(RotateZSlider, 0.5);
            _geometryDrawer = temp;
            _geometryDrawer.DrawAll();
            Thread.Sleep(10);
        };
        _animationTimer.Interval = new TimeSpan(10);
    }

    private void BtnAnimateClick(object sender, RoutedEventArgs e) {
        if (_animationTimer.IsEnabled) {
            _animationTimer.Stop();
        } else {
            _animationTimer.Start();
        }
    }


    private void OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
        _geometryDrawer?.DrawAll();
    }

    private void OnRbtValueChanged(object sender, RoutedEventArgs e) {
        _geometryDrawer?.DrawAll();
    }


    private void PerspectiveProjection_OnChecked(object sender, RoutedEventArgs e) {
        if (RbtPerspective.IsChecked.Equals(true) && _initEnded) {
            _geometryDrawer.ProjectionType = ProjectionType.Perspective;
            _geometryDrawer.DrawAll();
        }
    }

    private void IsometricProjection_OnChecked(object sender, RoutedEventArgs e) {
        if (RbtIsometric.IsChecked.Equals(true) && _initEnded) {
            _geometryDrawer.ProjectionType = ProjectionType.Isometric;
            _geometryDrawer.DrawAll();
        }
    }

    private void ChbRotateAfterShift_OnChecked(object sender, RoutedEventArgs e) {
        _geometryDrawer.DrawAll();
    }

    private void ChbFillWithColor_OnChecked(object sender, RoutedEventArgs e) {
        _geometryDrawer?.DrawAll();
    }

    private void DisableAllExceptCurrent(ToggleButton current) {
        foreach (var item in MyToolbar.Items) {
            if (item is ToggleButton button && button != current) {
                button.IsChecked = false;
            }
        }
    }


    private void ObjectSelected(object sender, RoutedEventArgs e) {
        if (_initEnded && sender is ToggleButton button && _geometryDrawer != null) {
            DisableAllExceptCurrent(button);
            int rad = Math.Max(Math.Min(TbRad.Text.intOrDefault(200), 2000), 1);
            int segm = Math.Max(Math.Min(TbSegments.Text.intOrDefault(2), 200), 2);
            if (button == TbCube) {
                _geometryDrawer.LoadCube();
            } else if (button == TbIntersection) {
                _geometryDrawer.LoadIntersection();
            } else if (button == TbIcosahedron) {
                _geometryDrawer.LoadIcosahedron();
            } else if (button == TbSphere) {
                _geometryDrawer.LoadSphere(rad, segm);
            } else if (button == TbGarlic) {
                _geometryDrawer.LoadGarlic(rad, segm);
            } else if (button == TbOctahedron) {
                _geometryDrawer.LoadOctahedron();
            } else if (button == TbDodecahedron) {
                _geometryDrawer.LoadDodecahedron();
            } else if (button == TbTorus) {
                _geometryDrawer.LoadTorus(rad, rad / 2, segm);
            } else if (button == TbSpiral) {
                _geometryDrawer.LoadSpiral(rad, rad / 3, segm / 2, rad / 5);
            } else {
                return;
            }
            _geometryDrawer.DrawAll();
        }
    }

    private void ChbRemoveInvisibleFaces_OnChecked(object sender, RoutedEventArgs e) {
        _geometryDrawer?.DrawAll();
    }

    private void SphereSetupChanged(object sender, TextChangedEventArgs e) {
        foreach (var item in MyToolbar.Items) {
            if (item is ToggleButton tb && tb.IsChecked.Equals(true)) {
                ObjectSelected(item, null);
            }
        }

        ObjectSelected(sender, e);
    }

    private void BtnAboutClick(object sender, RoutedEventArgs e) {
        MessageBox.Show(@"
Made by Pertsukhov Pavel, Higher School of Economics, Computer Graphics course, 2019
Used C#, Rider IDE compatible with Visual Studio 2017

You can rotate the images by pressing up/down/left/right keyboard buttons.
If they don't work properly, you should click on any radio-button, or reselect any figure.
Also you can use shortcuts:
Ctrl+R for reset button
Ctrl+A for animate button

Setting 'Rotate after shift' can be used to apply the rotation matrix after the shift matrix.
Setting 'Remove invisible faces' implements Roberts visibility algorithm.  

Settings 'Radius' and 'Segments' can be applied only for Sphere, Garlic, Torus, Spiral.   

The figure 'Intersection' is made to test z-Buffer privileges when the faces are intersected.
With other figures z-buffer works slower, because of sequential drawing pixel by pixel.
  
",
            "About");

    }
}
}