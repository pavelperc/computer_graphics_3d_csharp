﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Csharp3d {
// helper class to draw on canvas
class CanvasDrawer {
    private PictureBox _pictureBox;
    public int CenterX;
    public int CenterY;

    private Point center;

    Bitmap bmScreen;
    Graphics zbGraphics;
    Graphics grScreen;
    public Bitmap zBuffer;


    public CanvasDrawer(PictureBox pictureBox) {
        _pictureBox = pictureBox;
        CenterX = (int) (_pictureBox.Width / 2);
        CenterY = (int) (_pictureBox.Height / 2);

        bmScreen = new Bitmap(_pictureBox.Width, _pictureBox.Height);
        zBuffer = new Bitmap(_pictureBox.Width, _pictureBox.Height);

        grScreen = Graphics.FromImage(bmScreen);
        _pictureBox.Image = bmScreen;

        CenterX = _pictureBox.Width / 2;
        CenterY = _pictureBox.Height / 2;

        zbGraphics = Graphics.FromImage(zBuffer);
        zbGraphics.Clear(Color.FromArgb(Int32.MinValue));
    }

    // takes point[i1] and point[i2] from 2d array and draws a 2d line
    public void DrawLine(double[,] points, int i1, int i2, Pen pen) {
        grScreen.DrawLine(pen,
            (int) points[i1, 0],
            (int) points[i1, 1],
            (int) points[i2, 0],
            (int) points[i2, 1]);
    }

    private readonly Font _font = new Font("Arial", 12);

    // i is the first index of points, which is [x,2]
    public void DrawText(String text, double[,] points, int i, Pen pen) {
        grScreen.DrawString(text, _font, pen.Brush, (float) points[i, 0], (float) points[i, 1]);
    }

    public void DrawCircle(Pen pen, int x, int y, int rad) {
        grScreen.FillEllipse(pen.Brush, x - rad, y - rad, 2 * rad, 2 * rad);
    }

    // points of [x,2]
    public void DrawPoint(Pen pen, double[,] points, int i) {
        DrawCircle(pen, (int) points[i, 0], (int) points[i, 1], 5);
    }

    public void MoveToCenter(TransMatr transMatr) {
        transMatr.AddShift(CenterX, CenterY, 0.0);
    }

    public void ClearAll() {
        grScreen.Clear(Color.White);
        zbGraphics.Clear(Color.FromArgb(Int32.MinValue));
    }

    public void Submit() {
        _pictureBox.Refresh();
    }

    private Point[] _trinaglePoints = new Point[3];

    public void DrawTriangle(double[,] points, int i1, int i2, int i3, Pen pen) {
        _trinaglePoints[0] = new Point((int) points[i1, 0], (int) points[i1, 1]);
        _trinaglePoints[1] = new Point((int) points[i2, 0], (int) points[i2, 1]);
        _trinaglePoints[2] = new Point((int) points[i3, 0], (int) points[i3, 1]);

        grScreen.FillPolygon(pen.Brush, _trinaglePoints);
    }

    public void DrawTriangleWithZBuffer(double[,] points, int i1, int i2, int i3, Pen pen) {
        int x1 = (int) points[i1, 0];
        int y1 = (int) points[i1, 1];
        int z1 = (int) points[i1, 2];
        int x2 = (int) points[i2, 0];
        int y2 = (int) points[i2, 1];
        int z2 = (int) points[i2, 2];
        int x3 = (int) points[i3, 0];
        int y3 = (int) points[i3, 1];
        int z3 = (int) points[i3, 2];

        int yLower = Math.Min(Math.Min(y1, y2), y3);
        int yUpper = Math.Max(Math.Max(y1, y2), y3);

        // iterate horizontal lines
        // skip the first, because in FindIntersection we ignore the lowest boundaries.
        for (int y = yLower + 1; y <= yUpper; y++) {
            // searching drawing line coords
            int tempX;
            int tempZ;
            int startX = 0;
            int startZ = 0;
            int endX = 0;
            int endZ = 0;
            bool findStart = false;
            bool findEnd = false;
            if (FindIntersection(x1, y1, z1, x2, y2, z2, y, out tempX, out tempZ)) {
                findStart = true;
                startX = tempX;
                startZ = tempZ;
            }
            if (FindIntersection(x2, y2, z2, x3, y3, z3, y, out tempX, out tempZ)) {
                if (!findStart) {
                    findStart = true;
                    startX = tempX;
                    startZ = tempZ;
                } else {
                    findEnd = true;
                    endX = tempX;
                    endZ = tempZ;
                }
            }
            if (!findStart) {
                Console.WriteLine(
                    $"zBuffer Warning!! Not found startX for points {x1} {y1} {x2} {y2} {x3} {y3} on y={y}");
                continue;
            }
            if (!findEnd && FindIntersection(x3, y3, z3, x1, y1, z1, y, out tempX, out tempZ)) {
                findEnd = true;
                endX = tempX;
                endZ = tempZ;
            }
            if (!findEnd) {
                Console.WriteLine(
                    $"zBuffer Warning!! Not found endX for points {x1} {y1} {x2} {y2} {x3} {y3} on y={y}");
                continue;
            }
            // swap to sort
            if (startX > endX) {
                tempX = startX;
                startX = endX;
                endX = tempX;
                tempZ = startZ;
                startZ = endZ;
                endZ = tempZ;
            }

            for (int x = startX; x <= endX; x++) {
                int z = startX == endX ? startZ : startZ + (x - startX) * (endZ - startZ) / (endX - startX);

                if (x >= 0 && x < zBuffer.Width && y >= 0 && y < zBuffer.Height
                    && zBuffer.GetPixel(x, y).ToArgb() < z) {
                    zBuffer.SetPixel(x, y, Color.FromArgb(z));
                    bmScreen.SetPixel(x, y, pen.Color);
                }
                // else don't draw
            }
        }
    }

    // return true if intersection found, or false if intersection is out of segment.
    private bool FindIntersection(int x1, int y1, int z1, int x2, int y2, int z2, int y, out int x, out int z) {
        x = x1;
        z = z1;
        if (y1 == y2) {
            return false;
        }
        x = x1 + (y - y1) * (x2 - x1) / (y2 - y1);
        z = z1 + (y - y1) * (z2 - z1) / (y2 - y1);

//        Console.WriteLine($"Intersection on {x1} {y1} {x2} {y2} with y={y} is {x}");
        // take only the upper boundary point
        if (y <= Math.Min(y1, y2)) {
            return false;
        }
        if (y > Math.Max(y1, y2)) {
            return false;
        }
        return true;
    }
}
}