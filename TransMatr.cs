﻿using System;

namespace Csharp3d {
// Transform matrix 4 x 4 
class TransMatr {
    private double[,] _coords = new double[4, 4];

    // for matrix computations
    private double[,] _coordsCache = new double[4, 4];

    private static double[,] IdMatrix() => new double[4, 4] {
        {1, 0, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 1, 0},
        {0, 0, 0, 1}
    };

    // cached STATIC matrices for applying transformations on matrices
    private static double[,] _projectionMatrix = IdMatrix();
    private static double[,] _shiftMatrix = IdMatrix();
    private static double[,] _rotateXMatrix = IdMatrix();
    private static double[,] _rotateYMatrix = IdMatrix();
    private static double[,] _rotateZMatrix = IdMatrix();
    private static double[,] _scaleMatrix = IdMatrix();

    // makes this matrix identity
    public void ResetMatrix() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                _coords[i, j] = i == j ? 1 : 0;
            }
        }
    }

    // Creates identity matrix
    public TransMatr() {
        ResetMatrix();
    }

    public static TransMatr Identity() => new TransMatr();

    // gets an array [X,4] of 3d points (x, y, z, h)
    // and divides x, y, z by h
    // assumes that h != 0
    static void NormalizePoints(double[,] points) {
        for (int i = 0; i < points.L0(); i++) {
            points[i, 0] /= points[i, 3];
            points[i, 1] /= points[i, 3];
            points[i, 2] /= points[i, 3];
        }
    }

    private static double[,] _perspectiveProjectionMatrix = new double[4, 4] {
        {1, 0, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 1, -0.001},
        {0, 0, 0, 1}
    };

    private static double[,] _isometricProjectionMatrix = new double[4, 4] {
        {0.707,  -0.408, 0, 0},
        {0,       0.816, 0, 0},
        {-0.707, -0.408, 1, 0},
        {0,       0,     0, 1}
    };

    private void Swap(ref double[,] matr1, ref double[,] matr2) {
        double[,] temp = matr1;
        matr1 = matr2;
        matr2 = temp;
    }

    // One point perspective
    public void AddPerspectiveProjection() {
        _projectionMatrix = _perspectiveProjectionMatrix;
        MultCoords(_projectionMatrix);
    }

    public void AddIsometricProjection() {
        _projectionMatrix = _isometricProjectionMatrix;
        MultCoords(_projectionMatrix);
    }

    public TransMatr(double[,] otherCoords) {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                _coords[i, j] = otherCoords[i, j];
            }
        }
    }

    public TransMatr Copy() {
        return new TransMatr(_coords);
    }

    // mult left and normalize
    public void MultLeftAndNormalize(double[,] matrix, double[,] result) {
        Matr.Mult(matrix, _coords, result);
        NormalizePoints(result);
    }

    public void AddScale(double scale) {
        _scaleMatrix[0, 0] = scale;
        _scaleMatrix[1, 1] = scale;
        _scaleMatrix[2, 2] = scale;
        MultCoords(_scaleMatrix);
    }

    // mults right _coords on matr
    private void MultCoords(double[,] matr) {
        Matr.Mult(_coords, matr, _coordsCache);
        Swap(ref _coords, ref _coordsCache);
    }


    public void AddShift(double x, double y, double z) {
        _shiftMatrix[3, 0] = x;
        _shiftMatrix[3, 1] = y;
        _shiftMatrix[3, 2] = z;
        MultCoords(_shiftMatrix);
    }

    public void AddRotateX(double angle) {
        angle = angle * Math.PI * 2 / 360;
        _rotateXMatrix[1, 1] = Math.Cos(angle);
        _rotateXMatrix[1, 2] = Math.Sin(angle);
        _rotateXMatrix[2, 1] = -Math.Sin(angle);
        _rotateXMatrix[2, 2] = Math.Cos(angle);
        MultCoords(_rotateXMatrix);
    }

    public void AddRotateY(double angle) {
        angle = angle * Math.PI * 2 / 360;
        _rotateYMatrix[0, 0] = Math.Cos(angle);
        _rotateYMatrix[0, 2] = -Math.Sin(angle);
        _rotateYMatrix[2, 0] = Math.Sin(angle);
        _rotateYMatrix[2, 2] = Math.Cos(angle);
        MultCoords(_rotateYMatrix);
    }

    public void AddRotateZ(double angle) {
        angle = angle * Math.PI * 2 / 360;
        _rotateZMatrix[0, 0] = Math.Cos(angle);
        _rotateZMatrix[0, 1] = Math.Sin(angle);
        _rotateZMatrix[1, 0] = -Math.Sin(angle);
        _rotateZMatrix[1, 1] = Math.Cos(angle);
        MultCoords(_rotateZMatrix);
    }
}
}