﻿using System;

namespace Csharp3d {
static class Objects {
    private static double[,] Scale(this double[,] vertices, int factor) {
        for (int i = 0; i < vertices.L0(); i++) {
            vertices[i, 0] *= factor;
            vertices[i, 1] *= factor;
            vertices[i, 2] *= factor;
        }
        return vertices;
    }

    public static double[,] CubeVertices = new double[,] {
        {-1, -1, -1, 1},
        {-1, 1, -1, 1},
        {1, 1, -1, 1},
        {1, -1, -1, 1},

        {-1, -1, 1, 1},
        {-1, 1, 1, 1},
        {1, 1, 1, 1},
        {1, -1, 1, 1}
    }.Scale(100);

    // bottom means, that z is the height
    public static int[,] CubeFaces = {
//        bottom 1 0 3 2
        {1, 0, 3}, {3, 2, 1},
        // top 4 5 6 7
        {4, 5, 6}, {6, 7, 4},
        // front 0 4 7 3
        {0, 4, 7}, {7, 3, 0},
        // left 1 5 4 0
        {1, 5, 4}, {4, 0, 1},
        // right 3 7 6 2
        {3, 7, 6}, {6, 2, 3},
        // back 2 6 5 1
        {2, 6, 5}, {5, 1, 2},
    };
    
    // copied from cube vertices
    public static double[,] IntersectionVertices = new double[,] {
        {-1, -1, -1, 1},
        {-1, 1, -1, 1},
        {1, 1, -1, 1},
        {1, -1, -1, 1},

        {-1, -1, 1, 1},
        {-1, 1, 1, 1},
        {1, 1, 1, 1},
        {1, -1, 1, 1}
    }.Scale(100);
    
    public static int[,] IntersectionFaces = {
        {1, 6, 7}, {2, 5, 4},
        {1, 7, 6}, {2, 4, 5},
    };

    public static double[,] IcosahedronVertices = new double[12, 4] {
        {Math.Cos(2 * Math.PI * 0 / 10), Math.Sin(2 * Math.PI * 0 / 10), -0.5, 1},
        {Math.Cos(2 * Math.PI * 1 / 10), Math.Sin(2 * Math.PI * 1 / 10), 0.5, 1},
        {Math.Cos(2 * Math.PI * 2 / 10), Math.Sin(2 * Math.PI * 2 / 10), -0.5, 1},
        {Math.Cos(2 * Math.PI * 3 / 10), Math.Sin(2 * Math.PI * 3 / 10), 0.5, 1},
        {Math.Cos(2 * Math.PI * 4 / 10), Math.Sin(2 * Math.PI * 4 / 10), -0.5, 1},
        {Math.Cos(2 * Math.PI * 5 / 10), Math.Sin(2 * Math.PI * 5 / 10), 0.5, 1},
        {Math.Cos(2 * Math.PI * 6 / 10), Math.Sin(2 * Math.PI * 6 / 10), -0.5, 1},
        {Math.Cos(2 * Math.PI * 7 / 10), Math.Sin(2 * Math.PI * 7 / 10), 0.5, 1},
        {Math.Cos(2 * Math.PI * 8 / 10), Math.Sin(2 * Math.PI * 8 / 10), -0.5, 1},
        {Math.Cos(2 * Math.PI * 9 / 10), Math.Sin(2 * Math.PI * 9 / 10), 0.5, 1},
        {0, 0, -Math.Sqrt(5) / 2, 1},
        {0, 0, Math.Sqrt(5) / 2, 1},
    }.Scale(100);

    public static int[,] IcosahedronFaces = new int[,] {
        // cilinder
        {0, 1, 2},
        {2, 1, 3},
        {2, 3, 4},
        {4, 3, 5},
        {4, 5, 6},
        {6, 5, 7},
        {6, 7, 8},
        {8, 7, 9},
        {8, 9, 0},
        {0, 9, 1},
        // down
        {10, 0, 2},
        {10, 2, 4},
        {10, 4, 6},
        {10, 6, 8},
        {10, 8, 0},
        // top
        {11, 3, 1},
        {11, 5, 3},
        {11, 7, 5},
        {11, 9, 7},
        {11, 1, 9},
    };

    // dodecahedron:
    // https://api.tinkercad.com/libraries/1vxKXGNaLtr/0/docs/topic/Platonic+Solid+Generator+-+Part+2.html

    private static double phi = (1 + Math.Sqrt(5)) / 2.0;
    private static double iphi = 1.0 / phi;

    public static double[,] DodecahedronVertices = new double[,] {
        // cube bottom
        {-1, -1, -1, 1},
        {1, -1, -1, 1},
        {1, 1, -1, 1},
        {-1, 1, -1, 1},
        // cube top
        {-1, -1, 1, 1},
        {1, -1, 1, 1},
        {1, 1, 1, 1},
        {-1, 1, 1, 1},
        // bottom top
        {0, -iphi, -phi, 1},
        {0, iphi, -phi, 1},
        {0, -iphi, phi, 1},
        {0, iphi, phi, 1},
        // front back
        {-phi, 0, -iphi, 1},
        {-phi, 0, iphi, 1},
        {phi, 0, -iphi, 1},
        {phi, 0, iphi, 1},
        // left right
        {-iphi, -phi, 0, 1},
        {iphi, -phi, 0, 1},
        {-iphi, phi, 0, 1},
        {iphi, phi, 0, 1},
    }.Scale(100);

    private static int[,] GenerateDodecahedronFaces() {
        int[,] fifths = new int[,] {
            {0, 12, 3, 9, 8},
            {8, 9, 2, 14, 1},
            {9, 3, 18, 19, 2},
            {8, 1, 17, 16, 0},
            {12, 13, 7, 18, 3},
            {12, 0, 16, 4, 13},
            {1, 14, 15, 5, 17},
            {14, 2, 19, 6, 15},
            {13, 4, 10, 11, 7},
            {6, 11, 10, 5, 15},
            {7, 11, 6, 19, 18},
            {16, 17, 5, 10, 4}
        };
        int iFace = 0;
        int[,] faces = new int[36, 3];
        for (int i = 0; i < fifths.L0(); i++) {
            int i1 = fifths[i, 0];
            int i2 = fifths[i, 1];
            int i3 = fifths[i, 2];
            int i4 = fifths[i, 3];
            int i5 = fifths[i, 4];

            faces[iFace, 0] = i1;
            faces[iFace, 1] = i2;
            faces[iFace, 2] = i3;
            iFace++;
            faces[iFace, 0] = i3;
            faces[iFace, 1] = i4;
            faces[iFace, 2] = i5;
            iFace++;
            faces[iFace, 0] = i1;
            faces[iFace, 1] = i3;
            faces[iFace, 2] = i5;
            iFace++;
        }
        return faces;
    }

    public static int[,] DodecahedronFaces = GenerateDodecahedronFaces();


    public static double[,] OctahedronVertices = new double[,] {
        {0, 0, -1, 1},
        {0, 0, 1, 1},

        {-1, 0, 0, 1},
        {0, 1, 0, 1},
        {1, 0, 0, 1},
        {0, -1, 0, 1},
    }.Scale(100);

    public static int[,] OctahedronFaces = {
        {1, 2, 3},
        {1, 3, 4},
        {1, 4, 5},
        {1, 5, 2},

        {0, 3, 2},
        {0, 4, 3},
        {0, 5, 4},
        {0, 2, 5},
    };


    public static void GenerateTorus(out double[,] vertices, out int[,] faces, int radMain = 200, int radCircle = 20,
        int stepsCount = 10) {
        vertices = new double[stepsCount * stepsCount * 4, 4];

        int iVert = 0;


        for (int verticalStep = 0; verticalStep < stepsCount * 2; verticalStep++) {
            double verticalAngle = 2 * Math.PI * verticalStep / (stepsCount * 2);
            for (int horizontalStep = 0; horizontalStep < stepsCount * 2; horizontalStep++) {
                double horizontalAngle = 2 * Math.PI * horizontalStep / (stepsCount * 2);

                double x = (radMain + radCircle * Math.Cos(horizontalAngle)) * Math.Cos(verticalAngle);
                double y = (radMain + radCircle * Math.Cos(horizontalAngle)) * Math.Sin(verticalAngle);
                double z = radCircle * Math.Sin(horizontalAngle);
                vertices[iVert, 0] = x;
                vertices[iVert, 1] = y;
                vertices[iVert, 2] = z;
                vertices[iVert, 3] = 1;
                iVert++;
            }
        }

        if (iVert != vertices.L0()) {
            Console.WriteLine(
                $"Warning!! generated torus points are {iVert}, vertices array height is {vertices.L0()}");
        }

        // generate faces

        faces = new int[vertices.L0() * 2, 3];
        // current vertex
        iVert = 0;
        int iFace = 0;
        // go over all vertices and draw diamonds
        for (int verticalStep = 0; verticalStep < stepsCount * 2; verticalStep++) {
            for (int horizontalStep = 0; horizontalStep < stepsCount * 2; horizontalStep++) {
                int indLeft = iVert;
                // maybe top pole
                int indDown = (iVert + stepsCount * 2) % vertices.L0();

                int indRight = horizontalStep == stepsCount * 2 - 1 ? iVert - stepsCount * 2 + 1 : iVert + 1;
                int indDownRight = horizontalStep == stepsCount * 2 - 1 ? indDown - stepsCount * 2 + 1 : indDown + 1;


                faces[iFace, 0] = indLeft;
                faces[iFace, 1] = indDownRight;
                faces[iFace, 2] = indDown;

                iFace++;
                faces[iFace, 0] = indLeft;
                faces[iFace, 1] = indRight;
                faces[iFace, 2] = indDownRight;
                iVert++;
                iFace++;
            }
        }

        if (iFace != faces.L0()) {
            Console.WriteLine(
                $"Warning!! generated torus faces are {iFace}, face array height is {faces.L0()}");
        }
    }

    
    // k is height shift
    public static void GenerateSpiral(out double[,] vertices, out int[,] faces, int radMain = 200, int radCircle = 20, int k = 40,
        int stepsCount = 10) {
        vertices = new double[stepsCount * stepsCount * 4 * 2, 4];

        int iVert = 0;


        for (int verticalStep = 0; verticalStep < stepsCount * 2 * 2; verticalStep++) {
            double verticalAngle = 2 * Math.PI * verticalStep / (stepsCount * 2);
            for (int horizontalStep = 0; horizontalStep < stepsCount * 2; horizontalStep++) {
                double horizontalAngle = 2 * Math.PI * horizontalStep / (stepsCount * 2);

                double x = (radMain + radCircle * Math.Cos(horizontalAngle)) * Math.Cos(verticalAngle);
                double y = (radMain + radCircle * Math.Cos(horizontalAngle)) * Math.Sin(verticalAngle);
                double z = radCircle * Math.Sin(horizontalAngle) + k * verticalAngle;
                vertices[iVert, 0] = x;
                vertices[iVert, 1] = y;
                vertices[iVert, 2] = z;
                vertices[iVert, 3] = 1;
                iVert++;
            }
        }

        if (iVert != vertices.L0()) {
            Console.WriteLine(
                $"Warning!! generated spiral points are {iVert}, vertices array height is {vertices.L0()}");
        }

        // generate faces

        faces = new int[vertices.L0() * 2 - stepsCount * 4, 3];
        // current vertex
        iVert = 0;
        int iFace = 0;
        // go over all vertices and draw diamonds
        for (int verticalStep = 0; verticalStep < stepsCount * 2 * 2 - 1; verticalStep++) {
            for (int horizontalStep = 0; horizontalStep < stepsCount * 2; horizontalStep++) {
                int indLeft = iVert;
                // maybe top pole
                int indDown = (iVert + stepsCount * 2);

                int indRight = horizontalStep == stepsCount * 2 - 1 ? iVert - stepsCount * 2 + 1 : iVert + 1;
                int indDownRight = horizontalStep == stepsCount * 2 - 1 ? indDown - stepsCount * 2 + 1 : indDown + 1;


                faces[iFace, 0] = indLeft;
                faces[iFace, 1] = indDownRight;
                faces[iFace, 2] = indDown;

                iFace++;
                faces[iFace, 0] = indLeft;
                faces[iFace, 1] = indRight;
                faces[iFace, 2] = indDownRight;
                iVert++;
                iFace++;
            }
        }

        if (iFace != faces.L0()) {
            Console.WriteLine(
                $"Warning!! generated spiral faces are {iFace}, face array height is {faces.L0()}");
        }
    }
    

    public static void GenerateSphere(out double[,] vertices, out int[,] faces, int radius = 200,
        int stepsCount = 10) {
        vertices = new double[(stepsCount - 1) * stepsCount * 2 + 2, 4];

        int iVert = 0;
        // add poles:
        vertices[iVert, 0] = 0;
        vertices[iVert, 1] = 0;
        vertices[iVert, 2] = 1;
        vertices[iVert, 3] = 1;
        iVert++;
        vertices[iVert, 0] = 0;
        vertices[iVert, 1] = 0;
        vertices[iVert, 2] = -1;
        vertices[iVert, 3] = 1;
        iVert++;

        // start from 1!!
        // generate points, except poles

        for (int verticalStep = 1; verticalStep < stepsCount; verticalStep++) {
            double verticalAngle = Math.PI * verticalStep / stepsCount;
            for (int horizontalStep = 0; horizontalStep < stepsCount * 2; horizontalStep++) {
                double horizontalAngle = 2 * Math.PI * horizontalStep / (stepsCount * 2);

                double x = Math.Sin(verticalAngle) * Math.Cos(horizontalAngle);
                double y = Math.Sin(verticalAngle) * Math.Sin(horizontalAngle);
                double z = Math.Cos(verticalAngle);
                vertices[iVert, 0] = x;
                vertices[iVert, 1] = y;
                vertices[iVert, 2] = z;
                vertices[iVert, 3] = 1;
                iVert++;
            }
        }
        vertices.Scale(radius);
        if (iVert != vertices.L0()) {
            Console.WriteLine(
                $"Warning!! generated sphere points are {iVert}, vertices array height is {vertices.L0()}");
        }

        GenerateSphereFaces(out faces, stepsCount, vertices.L0());
    }

    private static void GenerateSphereFaces(out int[,] faces, int stepsCount, int verticesCount) {
        // fill faces except poles:
        faces = new int[(verticesCount - 2 - stepsCount) * 2 + 2 * stepsCount, 3];
        // current vertex
        int iVert = 2;
        int iFace = 0;
        // go over all vertices and draw diamonds
        for (int verticalStep = 1; verticalStep < stepsCount; verticalStep++) {
            for (int horizontalStep = 0; horizontalStep < stepsCount * 2; horizontalStep++) {
                // a diamond:
                // |\
                // |_\
                // \ |
                //  \|

                int indLeft = iVert;
                // maybe top pole
                int indUp = verticalStep == 1 ? 0 : iVert - stepsCount * 2;
                // go a round -1 left or go one right
                int indRight = horizontalStep == stepsCount * 2 - 1 ? iVert - (stepsCount * 2 - 1) : iVert + 1;

                // maybe down pole
                // go down and one right. or just go to the next round, if we at the end of the horizontal round
                int indDown =
                    verticalStep == stepsCount - 1
                        ? 1
                        : horizontalStep == stepsCount * 2 - 1
                            ? iVert + 1
                            : iVert + stepsCount * 2 + 1;


                faces[iFace, 0] = indLeft;
                faces[iFace, 1] = indUp;
                faces[iFace, 2] = indRight;

                iFace++;
                faces[iFace, 0] = indLeft;
                faces[iFace, 1] = indRight;
                faces[iFace, 2] = indDown;
                iVert++;
                iFace++;
            }
        }

        if (iFace != faces.L0()) {
            Console.WriteLine($"Warning!! generated sphere faces are {iVert}, faces array height is {verticesCount}");
        }
    }

    public static void GenerateGarlic(out double[,] vertices, out int[,] faces, int radius = 200,
        int stepsCount = 10) {
        vertices = new double[(stepsCount - 1) * stepsCount * 2 + 2, 4];

        int iVert = 0;
        // add poles:
        vertices[iVert, 0] = 0;
        vertices[iVert, 1] = 0;
        vertices[iVert, 2] = 1;
        vertices[iVert, 3] = 1;
        iVert++;
        vertices[iVert, 0] = 0;
        vertices[iVert, 1] = 0;
        // garlic
        vertices[iVert, 2] = -1 - Math.Pow(Math.PI / 3, 5);
        ;
        vertices[iVert, 3] = 1;
        iVert++;

        // start from 1!!
        // generate points, except poles

        for (int verticalStep = 1; verticalStep < stepsCount; verticalStep++) {
            // beta
            double verticalAngle = Math.PI * verticalStep / stepsCount;
            for (int horizontalStep = 0; horizontalStep < stepsCount * 2; horizontalStep++) {
                // alpha
                double horizontalAngle = 2 * Math.PI * horizontalStep / (stepsCount * 2);
                double x = Math.Sin(verticalAngle) * Math.Cos(horizontalAngle);
                double y = Math.Sin(verticalAngle) * Math.Sin(horizontalAngle);
                double z = Math.Cos(verticalAngle);

//                garlic:
                x = x * (1.0 + 0.5 * Math.Abs(Math.Sin(2 * horizontalAngle)));
                y = y * (1.0 + 0.5 * Math.Abs(Math.Sin(2 * horizontalAngle)));
                if (verticalAngle > 0) {
                    z = z - Math.Pow(verticalAngle / 3, 5);
                }

                vertices[iVert, 0] = x;
                vertices[iVert, 1] = y;
                vertices[iVert, 2] = z;
                vertices[iVert, 3] = 1;
                iVert++;
            }
        }
        vertices.Scale(radius);
        if (iVert != vertices.L0()) {
            Console.WriteLine(
                $"Warning!! generated sphere points are {iVert}, vertices array height is {vertices.L0()}");
        }

        GenerateSphereFaces(out faces, stepsCount, vertices.L0());
    }
}
}