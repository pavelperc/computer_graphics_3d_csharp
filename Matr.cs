﻿using System;

namespace Csharp3d {
static class Matr {
    // m1[a x b] * m2[b x c] = m3[a x c]
    public static void Mult(double[,] m1, double[,] m2, double[,] result) {
        int a = m1.L0();
        int b = m1.L1();
        int b1 = m2.L0();
        int c = m2.L1();
        if (b != b1) {
            throw new ArgumentException($"Can not multiply matrices of sizes {a} x {b} and {b1} x {c}");
        }
        if (result.L0() != a || result.L1() != c) {
            throw new ArgumentException($"Can not multiply matrices of sizes {a} x {b} and {b1} x {c}, " +
                                        $"because the result matrix is {result.L0()} x {result.L1()}");
        }

        for (int iA = 0; iA < a; ++iA) {
            for (int iC = 0; iC < c; ++iC) {
                // computing one cell
                double cell = 0;
                for (int iB = 0; iB < b; ++iB) {
                    cell += m1[iA, iB] * m2[iB, iC];
                }
                result[iA, iC] = cell;
            }
        }
    }
}
}